package main

import (
	"net/http"
	"server/database"
	"server/routes"

	"github.com/gorilla/handlers"
)

/*STARTS HTTP SERVER*/

func main() {
	database.InitDB()

	router := routes.NewRouter()

	//cors headers
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	origins := handlers.AllowedOrigins([]string{"http://localhost:5173"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"})

	http.ListenAndServe(":8080", handlers.CORS(headers, origins, methods)(router))
}
