import React, {useState, useEffect} from "react";

//to list users in the grid in the main view
import {getAllUsersFromDb} from "../api/apiHelper.js";
import {Link} from "react-router-dom";

const UserGrid = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => { //assigns users to a state when component did mount
        const fetchUsers = async () => {
            const users = await getAllUsersFromDb();
            setUsers(users);
        };
        fetchUsers();
        console.log(users)
    }, []);

    const [selectedUser, setSelectedUser] = useState(null); //if a row is selected from the user table

    const handleRowClick = (user) => {
        setSelectedUser(user); //when click a user on grid, sets clicked user to selected user
    };

    return (<div className="container mx-auto p-32 w-3/4">
        <h1 className="text-xl font-bold mb-4">FillLabs Human Resources</h1>
        <div className="flex justify-between mb-4 ">
            <Link
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                to="/user-detail/new"
            >
                New
            </Link>
            <div>
                {/*for edit and delete buttons, if selected user is null, a non-clickable span is rendered instead of a link*/}
                {selectedUser ? (
                    <Link
                        className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mr-2"
                        to="/user-detail/edit"
                        state={selectedUser}
                    >
                        Edit
                    </Link>
                ) : (
                    <span className="bg-gray-400 text-white font-bold py-2 px-4 rounded mr-2 cursor-not-allowed">
                        Edit
                    </span>
                )}

                {selectedUser ? (<Link
                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                    to="/user-detail/delete"
                    state={selectedUser}
                >
                    Delete
                </Link>) : (
                    <span className="bg-gray-400 text-white font-bold py-2 px-4 rounded cursor-not-allowed">
                        Delete
                    </span>)}
            </div>
        </div>
        <table className="w-full">
            <thead>
            <tr className="bg-gray-400 text-black">
                <th className="py-2 px-4 text-left">ID</th>
                <th className="py-2 px-4 text-left">Name</th>
                <th className="py-2 px-4 text-left">Email</th>
            </tr>
            </thead>
            <tbody>
            {/*mapping user list getting from api*/}
            {users.map((user) => (<tr
                key={user.id}
                onClick={() => handleRowClick(user)}
                className={`${selectedUser === user ? " text-black" : ""} hover:bg-[#37306B] hover:text-white hover:rounded transition duration-500 cursor-pointer`}
            >
                <td className="py-4 border-b px-4">{user.id}</td>
                <td className="py-4 border-b px-4">{user.name}</td>
                <td className="py-4 border-b px-4">{user.email}</td>
            </tr>))}
            </tbody>
        </table>
    </div>);
};

export default UserGrid;
