import React, { useState } from "react";
import { useLocation, useParams, useNavigate } from "react-router-dom";
import {
  saveUserToDb,
  deleteUserToDb,
  updateUserToDb,
} from "../api/apiHelper.js";

const DetailForm = (props) => {
  const navigate = useNavigate();
  const location = useLocation();
  const user = location.state;
  const params = useParams();
  console.log(user);

  const [name, setName] = useState(user ? user.name : "");
  const [email, setEmail] = useState(user ? user.email : "");
  const [password, setPassword] = useState(user ? user.password : "");

  const createNewUser = () => {
    /*creates a user with the values in the form to perform operations on edit and delete pages*/
    let newUser = {
      name: name,
      email: email,
      password: password,
    };

    return newUser;
  };

  const buttonText = //changes the button text according to the parameter from url
    params.type === "new"
      ? "Create"
      : params.type === "edit"
      ? "Save"
      : params.type === "delete"
      ? "Delete"
      : "";

  /**/
  const saveUser = async () => {
    const newUser = createNewUser();
    try {
      await saveUserToDb(newUser);
      navigate("/"); //redirects to the main route when the operation is complete
    } catch (error) {
      alert("an error occured");
    }
  };


  const deleteUser = async () => {
    const id = user.id;
    try {
      await deleteUserToDb(id);
      navigate("/");
    } catch (error) {
      console.log(error)

      alert("an error occured : ", error);
    }
  };
  const updateUser = async () => {
    const id = user.id;
    const newUser = createNewUser();

    try {
      await updateUserToDb(id, newUser);
      navigate("/");
    } catch (error) {
      console.log(error)
      alert("an error occured : ", error);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center p-10">
      <form className="w-full max-w-lg ">
        <div className="flex mb-6">
          <div className="w-full md:w-2/3 m-2">
            <label
              className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
              htmlFor="name"
            >
              Name:
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              id="name"
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
        </div>
        <div className="flex mb-6 ">
          <div className="w-full md:w-1/3 m-2">
            <label
              className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
              htmlFor="email"
            >
              Email:
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="w-full md:w-2/3 m-2">
            <label
              className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
              htmlFor="password"
            >
              Password:
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
      </form>
      <button
        className="border-2 m-2 bg-green-500 ease-in-out hover:bg-white hover:border-green-700 hover:text-black transition duration-500  text-white font-bold py-2 px-4 rounded"
        onClick={() => {
          /*Here, according to the value of the buttonText variable, the function to be called by the button is decided.
This is what distinguishes update, delete, create from each other.*/
          if (buttonText == "Create") {
            saveUser();
          } else if (buttonText == "Save") {
            updateUser();
          } else if (buttonText == "Delete") {
            deleteUser();
          } else {
            console.log("An error occurred");
          }
        }}
      >
        {buttonText}
      </button>
    </div>
  );
};

export default DetailForm;
