import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import UserGrid from "./views/UserGrid";
import DetailForm from "./views/DetailForm.jsx";

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<UserGrid/>}/>
                {/*The type parameter specifies the operation to be performed on the opened page. like edit or create*/}
                <Route path="/user-detail/:type" element={<DetailForm/>}/>
            </Routes>
        </BrowserRouter>
    )
};

export default App;
