import axios from "axios";

/*
functions that send requests to api
*/

const baseUrl = "http://localhost:8080";

const getAllUsersFromDb = async () => {
  try {
    const response = await axios.get(`${baseUrl}/getAll`);
    return response.data;
  } catch (error) {
    throw new Error("Failed to fetch users");
  }
};

const getUserByIdFromDb = async (id) => {
  try {
    const response = await axios.get(`${baseUrl}/get/${id}`);
    return response.data;
  } catch (error) {
    throw new Error(`Failed to fetch user with id ${id}`);
  }
};

const saveUserToDb = async (user) => {
  try {
    const response = await axios.post(`${baseUrl}/create`, user);
    return response.data;
  } catch (error) {
    throw new Error(`Failed to save user`);
  }
};

const updateUserToDb = async (id, user) => {
  try {
    const response = await axios.put(`${baseUrl}/update/${id}`, user);
    return response.data;
  } catch (error) {
    throw new Error(`Failed to update user with id ${id}`);
  }
};

const deleteUserToDb = async (id) => {
  try {
    const response = await axios.delete(`${baseUrl}/delete/${id}`);
    return response.data;
  } catch (error) {
    throw new Error(`Failed to delete user with id ${id}`);
  }
};

export { getAllUsersFromDb, getUserByIdFromDb, saveUserToDb, updateUserToDb, deleteUserToDb };
