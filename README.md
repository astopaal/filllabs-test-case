# User Information and Management App

  
  

## Information About App

  

This web application is made by using client side react and server side golang technologies.

PostgreSQL is used for database operations.

  

## Tech Stack

  

- React

- TailwindCss

- Go

- PostgreSql

- Vite

  

## How to run

  

Run ``` git clone https://gitlab.com/astopaal/filllabs-test-case ``` in the terminal to copy the source code.

Run ``` npm install ```in the "question-4 / client" directory.
Run `go get ./...` in the "question-4 / server" directory.

After making sure the packages are installed successfully, run `go install server` in the server directory, and then run ` npm run dev` in the client directory. 

#### Important note : 
For database connections, you should create an .env file in the server directory and write the config data for the database you want. 

Please note that the back-end will work incorrectly without a database connection.