package main

import (
	"fmt"
	"sort"
)

func mostFrequent(wordList []string, n int) string {

	//we can solve this problem brute force by checking all elements in order.
	//but i don't want the time complexity to be o(n^2)
	//so let's first sort out the sequence

	sort.Strings(wordList)

	maxCount := 1      //count of the most frequently repeated string
	res := wordList[0] //most frequently repeated string
	currentCount := 1  //count of the string is repeated in the current iteration

	/* If the current word is the same as the next one, currentCount is incremented.
	This continues until the word changes.
	When the word changes, it is compared with max Count.
	*/

	for i := 1; i < n; i++ {
		if wordList[i] == wordList[i-1] {
			currentCount++
		} else {
			currentCount = 1
		}

		if currentCount > maxCount {
			maxCount = currentCount
			res = wordList[i-1]
		}
	}

	return res
}

func main() {
	wordList := []string{"apple", "pie", "apple", "red", "red", "red"}
	n := len(wordList)
	fmt.Println(mostFrequent(wordList, n))
}
