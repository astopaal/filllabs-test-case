package main

import (
	"fmt"     //pkg for print and scan
	"sort"    //this package sorts strings and ints e.g
	"strings" //simple string functions
)

func sortByACount(words []string) []string { //this function returns a list with the words in it sorted by the a count (desc)
	sort.Slice(words, func(i, j int) bool { //takes two arguments, a list and sort method, returns true or false
		aCountI := strings.Count(words[i], "a")
		aCountJ := strings.Count(words[j], "a")

		//Since words with no a in them also have 0 a's, we can think of them as having an equal a count.
		if aCountI != aCountJ {
			//the return operation here actually tells the sort function,
			//how to sort by giving it a true or false
			return aCountI > aCountJ
		} else {
			return len(words[i]) > len(words[j])
		}
	})

	return words
}

func main() {
	words := []string{"aaaasdas", "adaff", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	fmt.Println(sortByACount(words))
}
